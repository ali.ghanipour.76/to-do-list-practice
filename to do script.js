let btn = document.getElementById("done");
let toDo = document.getElementsByClassName("toDo")[0];
let done = document.getElementsByClassName("done")[0];
let toDoList = [];
let savedToDoList = localStorage.getItem("toDoList");
if (savedToDoList) {
  toDoList = JSON.parse(savedToDoList);
  toDoList.forEach(tableAddToDo);
}
btn.addEventListener("click", function (params) {
  toDo.style.transform = "translateX(-100%)";

  done.style.transform = "translateX(0)";
});
document.getElementById("toDo").addEventListener("click", function (params) {
  toDo.style.transform = "translateX(0)";
  done.style.transform = "translateX(100%)";
});
document.getElementById("addToDo").addEventListener("click", function (text) {
  text = document.getElementById("text").value;
  let object = { text: text, id: new Date().getTime() };
  toDoList.push(object);
  localStorage.setItem("toDoList", JSON.stringify(toDoList));
  tableAddToDo(object);
});
document.getElementById("task1").addEventListener("click", function (text) {
  let row = text.target.parentElement.parentElement;
  //   for removing row simple
  row.remove();
  //   for removing row the hard way
  //   let table = document.getElementById("table")
  //   table.childNodes[1].removeChild(row)
  //   table.firstChild.removeChild(row)

  // for using strike
  //   let td = row.childNodes[1];
  //   let strike = document.createElement("strike");
  //   strike.innerText = td.innerText;
  //   td.removeChild(td.childNodes[0]);
  //   td.appendChild(strike);
});
function tableAddToDo(object) {
  let tr = document.createElement("tr");
  let td = document.createElement("td");
  td.innerText = object.text;
  tr.appendChild(td);

  let button = document.createElement("button");
  button.innerText = "Done";
  button.onclick = tableDoneAction(object);
  td = document.createElement("td");
  td.appendChild(button);
  tr.appendChild(td);
  let table = document.getElementById("table");
  table.appendChild(tr);
}
function tableDoneAction(id) {
  return function (params) {
    console.log(id);
    let row = params.target.parentElement.parentElement;

     localStorage.setItem(
      "toDoList",
      JSON.stringify(
        JSON.parse(localStorage.getItem("toDoList")).filter(
          (params) => params.id !== id.id
        )
      )
    );


    row.remove();
  };
}
